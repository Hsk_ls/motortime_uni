import Vue from 'vue'
import App from './App'
import tabBar from "components/tabbar/tabbar.vue"

Vue.config.productionTip = false

App.mpType = 'app'
Vue.component('tab-Bar', tabBar)
const app = new Vue({
	...App
})
app.$mount()
