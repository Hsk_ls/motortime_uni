/**
 * 发送请求
 */
function baseRequest(url, method, data, {
	noAuth = false,
	noVerify = false
}) {
	let Url = 'https://api.motortime.com/v1/app/'
	// let Url = 'https://dev-api.motortimevip.com/v1/app/'
	let Authorization = 'Bearer' + ' ' + uni.getStorageSync('access_token');
	let header = {
		'content-type': 'application/json',
		'Authorization': Authorization
	}
	if(data){
		data.equipment_id = uni.getStorageSync('equipment_id') ? uni.getStorageSync('equipment_id') : 0
		data.version=uni.getSystemInfoSync().appVersion
	}
	
	return new Promise((reslove, reject) => {
		uni.request({
			url: Url + url,
			method: method || 'GET',
			header: header,
			data: data || {},
			success: (res) => {
				reslove(res.data);
			},
			fail: (msg) => {
				reject('请求失败');
			}
		})
	});
}

const request = {};

['options', 'get', 'post', 'put', 'head', 'delete', 'trace', 'connect'].forEach((method) => {
	request[method] = (api, data, opt) => baseRequest(api, method, data, opt || {})
});



export default request;
