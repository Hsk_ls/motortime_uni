import request from "@/utils/request.js";
let source, platform, appname
uni.getSystemInfo({
	success: function(res) {
		// #ifdef APP-PLUS||H5
		platform = res.platform
		appname = res.platform
		if (res.platform == 'ios') {
			source = 3
		} else {
			source = 7
		}
		// #endif
		// #ifdef MP-WEIXIN
		appname = 'weixin'
		platform = 1
		source = 4
		// #endif
		// #ifdef MP-TOUTIAO
		appname = 'toutiao'
		platform = 2
		source = 15 + '-' + appname
		// #endif
		// #ifdef MP-BAIDU
		appname = 'baidu'
		platform = 3
		source = 16 + '-' + appname
		// #endif
	}
});

//埋点
export function burialPoint(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	data.invitees_scene_id = uni.getStorageSync('Appoptions') ? uni.getStorageSync('Appoptions').scene : ''
	data.equipment_id = uni.getStorageSync('equipment_id') ? uni.getStorageSync('equipment_id') : 0
	return request.post('user/burial_point', data);
}
export function getShareTask(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('share-task', data);
}
//首页轮播
export function getSliderImg(data) {
	return request.get('home/sliders');
}
//首页弹窗
export function getAdvs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('advs', data);
}
//首页列表
export function getHomeList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('home/index-lists', data);
}
//频道栏目
export function getChannelList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/channel', data);
}
//资讯详情
export function getArticleInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id, data);
}
//资讯为您推荐
export function getRecommends(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id + '/recommends', data);
}
//资讯评论列表
export function getCommentlist(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id + '/comments', data);
}
//资讯点赞
export function articlePraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/praise', data);
}
//资讯一般般
export function articlenoPraise(data) {

	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/praise-no', data);
}
//资讯收藏
export function articleCollect(data) {

	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/collect', data);
}
//资讯评论
export function articleCommentpost(data) {

	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/comment', data);
}
//资讯评论点赞
export function articleCommPraise(data) {

	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/comment/praise', data);
}
//删除资讯评论
export function articleCommDel(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.delete('archive-comment/' + id + '/delete', data);
}
//资讯生成海报
export function articlePoster(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/share-poster', data);
}
//帖子生成海报
export function bbsPoster(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/share-poster', data);
}
//参与投票
export function voteChoose(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('vote/choose', data);
}
export function voteInfo(id) {
	return request.get('vote/' + id);
}
//帖子详情
export function getBbsInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/card/' + id, data);
}
//帖子评论列表
export function getBbsCommentlist(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/card/' + id + '/comments', data);
}
//帖子评论删除
export function bbsCommDel(id) {
	return request.delete('bbs/card-comment/' + id + '/delete');
}

//帖子点赞
export function bbsPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card/praise', data);
}
//帖子收藏
export function bbsCollect(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card/collect', data);
}
//帖子评论
export function bbsCommentpost(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card-comment', data);
}

//帖子评论点赞
export function bbsCommentPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card-comment/praise', data);
}
//话题列表
export function getTopicList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/lists', data);
}
//话题详情
export function getTopicInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/' + id, data);
}

//话题详情帖子列表
export function getTopicBbs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/cards', data);
}
//话题品牌
export function getTopicBrands() {
	return request.get('topic/brands');
}
//话题分类
export function getTopicCate(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/categories', data);
}
//创建话题
export function addTopic(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('topic/add', data);
}

//登录
export function motorLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('mini-login-new', data);
}
//app微信登录
export function motorappLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('app-new-login', data);
}
//app苹果登录
export function motorappAppleLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/ios-login', data);
}

//获取验证码
export function getCode(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('sms/code', data);
}
//手机号登录
export function motorphoneLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/login', data);
}
//绑定手机号
export function phoneBinding(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/phone_binding', data);
}
//绑定微信号
export function wechatBinding(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('app-weixin-bingding', data);
}
//用户信息
export function getMemberInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('user', data);
}
//用户信息
export function getUserInfo(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('user/' + id, data);
}
//保存用户信息
export function saveMemberInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('user', data);
}
//用户动态列表
export function getUserList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('space/' + data.user_id + '/lists', data);
}
//获取活动详情
export function getActiveInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/' + id + '/recruit-show', data);
}
//提交报名
export function submitActive(data) {
	console.log('submitActive')
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('huodong/recruit-enroll', data);
}
//获取打卡坐标点
export function getPoplist(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/' + id + '/recruit-coordinate', data);
}
//消息通知次数
export function getNotoceTem(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/template', data);
}
//发帖
export function postBbs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card', data);
}
//验证是否可以拍照打卡

export function getClocktest(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-clock-coordinate-testing', data);
}
//拍照打卡

export function getRecruit(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-coordinate-sign', data);
}
//获取其他用户信息（获赞、关注、粉丝数量）
export function getUserOtherInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('user-other', data);
}
//获取我的车辆列表

export function getCarlist(data) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('car/list', data);
}
//删除车辆
export function delCars(id) {
	return request.delete('car/' + id + '/delete');
}
//设为默认
export function defultCars(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('car/' + id + '/default');
}
//获取车辆品牌

export function getBrands() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('car/brands', data);
}
//获取车系
export function selectCar(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/cars', data);
}

//获取车型
export function selectModel(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/models', data);
}
//获取车详情
export function getCarinfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('car/' + id, data);
}
//提交车辆认证
export function submitCars(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('car/add', data);
}
//直播详情
export function getLiveInfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('live/' + id, data);
}
//直播状态
export function getLiveStatus(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('live/status/' + id, data);
}
//直播评论列表
export function getLiveComments(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('live/' + id + '/comments', data);
}
//直播发表评论
export function liveCommentpost(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('live/comment', data);
}
//分享内容
export function getShareImg(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('share-info', data);
}
//投票
export function getSurvey(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('surveyed/' + id, data);
}
//提交投票
export function postSurvey(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('surveyed/choose', data);
}
//提交反馈
export function toFeedback(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('surveyed/feedback', data);
}
//用户基础信息
export function getEquipment(data) {
	return request.post('user/equipment', data);
}
//商城列表
export function getGoodsChannels() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('shop/channels', data);
}
//商品品牌列表
export function getGoodsBrandList() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('shop/brands', data);
}
//首页商品分类
export function getShopCategories() {
	return request.get('shop/categories');
}
//商品分类列表
export function getShopClassList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/columns', data);
}
//商品类别
export function getShopSubList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/subs', data);
}
//商品列表
export function getShopList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/goods', data);
}
//商品详情
export function getGoodsInfo(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/good/' + id, data);
}
//商品规格
export function getGoodsAttr(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/attribute', data);
}
//加入购物车
export function goodsAddcarts(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/cart-add', data);
}
//购物车列表
export function getCartsList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/carts', data);
}
//购物车选中
export function setCartSele(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/cart-selection', data);
}
//删除购物车商品
export function delCart(id) {
	return request.delete('shop/cart-delete/' + id);
}
//获取默认地址
export function getDefaultAddress() {
	return request.get('shop/address-default');
}
//获取运费
export function getLogistics(url, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post(url, data);
}
//创建订单
export function orderAdd(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/order-add', data);
}
//地址列表
export function getAddressList() {
	return request.get('shop/addresses');
}
//删除地址
export function delAddr(id) {
	return request.delete('shop/address/' + id);
}
//添加地址
export function saveAddress(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/address', data);
}
//编辑地址
export function editAddress(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('shop/address/' + data.id, data);
}
//订单详情
export function getOrderinfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('shop/order-show/' + id, data);
}
//支付
export function goodsPayment(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/order-pay', data);
}
//订单列表
export function getOrderList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/order-list', data);
}
//取消订单
export function cancelOrder(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.put('shop/order-cancel/' + id, data);
}
//删除订单
export function delOrder(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.delete('shop/order-delete/' + id, data);
}
//确认收货
export function submitOrder(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.put('shop/order-sub/' + id, data);
}
//退款信息
export function getRefundinfo() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('shop/order-refund-info', data);
}
//退款
export function refundOrder(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('shop/order-refund/' + id, data);
}
//取消退款
export function cancelRefundOrder(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.put('shop/order-refund-cancel/' + id, data);
}
//生成团购订单
export function getGroupOrder(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/order-group-deposit-add', data);
}
//参加团购
export function groupSign(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/group-sign', data);
}
//我的帖子
export function getMyPostlist(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/my-cards', data);
}
//我的帖子草稿
export function getMyDrafts(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/my-drafts', data);
}
//帖子草稿详情
export function getDraftInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/drafts-info', data);
}
//删除草稿
export function delDrafts(id) {
	return request.delete('bbs-draft/' + id + '/delete');
}
//未读消息数量
export function getNoticenums(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/nums', data);
}
//评论消息
export function getCommentMessageList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/comments', data);
}
//系统消息
export function getAdminMessageList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/system', data);
}

//消息已读
export function readMessage(url) {
	return request.put(url);
}
//点赞消息
export function getPariseMessageList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/praise', data);
}
//点赞人列表
export function getPraiseUserList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/praise-user', data);
}
//我的收藏
export function getCollect(url, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get(url, data);
}
//评论详情
export function getCommentInfo(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive-comment/' + id, data);
}
//评论回复列表
export function getReplylist(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive-comment/' + id + '/replys', data);
}
//积分兑换
export function integralOrderadd(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('shop/integral-pay', data);
}
//我的积分
export function getMyIngetral(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('my/integral', data);
}
//帖子列表
export function getBbsList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/cards', data);
}
//地区列表
export function getAreaList() {
	return request.get('shop/areas-all');
}
//我的活动
export function getMyActive(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-lists', data);
}
//活动验证
export function Verification(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-code-verification', data);
}
//活动获取手机号
export function getActivePhone(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('mini-phone-decrypt', data);
}
//活动报名

export function activeApply(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('huodong/recruit-enroll', data);
}
// 问答
//问答话题
export function getAskTopicList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/topics', data);
}
//问答频道
export function getAskChannelList(id) {
	return request.get('ask/' + id + '/channel');
}
//问答列表
export function getQuesList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/lists', data);
}
//答案点赞
export function quesPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/praise', data);
}
//答案没帮助
export function quesPraiseNo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/praise-no', data);
}
//采纳答案
export function quesAdopt(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/adopt', data);
}
//问题详情
export function getQuesInfo(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/' + id, data);
}
//答案详情
export function getAskInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/' + data.id + '/answer', data);
}
//答案收藏
export function askCollect(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/collect', data);
}
//答案海报
export function askPoster(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/share-poster', data);
}
//答案评论列表
export function getAskCommentList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/' + data.id + '/comments', data);
}
//发表评论
export function askCommentpost(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/comment', data);
}
//删除评论
export function askCommDel(id) {
	return request.delete('ask-comment/' + id + '/delete');
}
//评论详情
export function getAskCommentInfo(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask-comment/' + id, data);
}
//评论列表
export function getAskReplylist(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask-comment/' + id + '/replys', data);
}
//评论点赞
export function askCommPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/comment/praise', data);
}
//回答问题
export function askAnswer(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/answer', data);
}
//提问问题
export function postQues(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('ask/question', data);
}
//获取答案草稿
export function getAskDraftInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/drafts-info', data);
}
//删除问题答案草稿
export function delQADrafts(id) {
	return request.delete('ask-draft/' + id + '/delete');
}
//删除问题
export function delQuestion(id) {
	return request.delete('ask-question/' + id + '/delete');
}
//删除答案
export function delAnswer(id) {
	return request.delete('ask-answer/' + id + '/delete');
}
//问题频道
export function getQuesChannelList() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('ask/channel-list', data);
}
//问题频道设置
export function getQuesChannelTow(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('ask/' + id + '/channel', data);
}
//问题标签
export function getQuesLableList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/labels', data);
}
//我的提问
export function getMyquestionList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/my-questions', data);
}
//我的答案
export function getMyanswerList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/space-answers', data);
}
//我的问答草稿
export function getMydraftsList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('ask/my-drafts', data);
}
//关注用户
export function followUser(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/follow', data);
}
//模板消息
export function noticeTemplate(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/template', data);
}
//关注列表
export function getFollowList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('my/follow', data);
}
//粉丝列表
export function getFansList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('my/fans', data);
}
//敏感词检测
export function checkText(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('check-text', data);
}
//搜索
export function getSearchList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('search/info', data);
}
//搜索历史
export function getSearchHistory(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('search/log', data);
}
//资讯列表
export function getArticleList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('home/lists', data);
}
//
export function getChannelArticleList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/lists', data);
}
//清除搜索记录
export function cleanHistory(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('search/log-delete', data);
}
//搜索阅读记录
export function getReadList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('search/browsing', data);
}
//清除阅读记录
export function cleanReadlist(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('search/browsing-delete', data);
}
//信息聚合新接口
export function getRecommendList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('home/index-recommend', data);
}
//id列表

export function getRecommendIdList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('home/index-recommend-all', data);
}
//id详情

export function getRecommendIdInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('home/index-recommend-info', data);
}
//分销
//提交分销认证
export function joinSalesclerk(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('commission/auth', data);
}
//分销认证详情
export function getSalesclerkInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('commission/info', data);
}

//分销商品列表
export function getSaleGoodsList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('commission/user-goods', data);
}
//分销订单列表
export function getSaleOrderList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('commission/order-list', data);
}
//分销商品详情
export function getSalesGoodsInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('commission/user-goods-info', data);
}
//获取更新版本
export function getVersionInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('setting/version', data);
}
